#include <lorawan.h>
#include <Wire.h>
#include <math.h>
#include <string.h>
/*#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <TinyGPS++.h>    */   
#include <LiquidCrystal_I2C.h>              

// OLED pins
/*#define OLED_SDA 21
#define OLED_SCL 22
#define OLED_RST 16
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels*/

//Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RST);
int lcdColumns = 20;
int lcdRows = 4;
int cursorRow = 0;
LiquidCrystal_I2C lcd(0x27, lcdColumns, lcdRows); 

const uint8_t payloadBufferLength = 64; // Adjust to fit max payload length
uint8_t payloadBuffer[payloadBufferLength];
unsigned long uplinkInterval = 20000;
unsigned long prevMillis = 0;

// OTAA credentials
const char *devEui = "9e4779d4bfd5b40d";
const char *appEui = "0000000000000000";
const char *appKey = "acf25845f4833d600c5e94eb4d5baeb6"; // msb

// define the pins used by the LoRa transceiver module
#define SCK 5
#define MISO 19
#define MOSI 27
#define SS 18
//#define RST 23
//#define DIO0 26

byte recvStatus = 0;
uint8_t outBuff[200];

static RTC_NOINIT_ATTR int SPREADING_FACTOR;

const sRFM_pins RFM_pins = {
    .CS = SS,
    .RST = 23,
    .DIO0 = 26,
    .DIO1 = 33,
    .DIO2 = 32,
    .DIO5 = -1,
};

bool find(int num, int array[], int size){
  for(int i = 0; i < size; i++){
    if(num == array[i]){
      return true;
    }
  }
  return false;
}


void setDataRate(int SF){
  if(SF == 7) lora.setDataRate(SF7BW125);
  else if(SF == 8) lora.setDataRate(SF8BW125);
  else if(SF == 9) lora.setDataRate(SF9BW125);
  else if(SF == 10) lora.setDataRate(SF10BW125);
  else if(SF == 11) lora.setDataRate(SF11BW125);
  else if(SF == 12) lora.setDataRate(SF12BW125);
}

void setNextSF(int minute){
  int sf7[] = {0,12,24,36,48};
  int sf8[] = {3,15,27,39,51};
  int sf10[] = {6,18,30,42,54};
  int sf11[] = {9,21,33,45,57};
  
  if(find(minute, sf7, 5)) SPREADING_FACTOR = 7;
  else if(find(minute, sf8, 5)) SPREADING_FACTOR = 8;
  else if(find(minute, sf10, 5)) SPREADING_FACTOR = 10;
  else if(find(minute, sf11, 5)) SPREADING_FACTOR = 11;
  else{
    while(minute % 3 != 0){
      if(minute == 0) minute = 59;
      else minute--;
    }
    setNextSF(minute);
    return;
  }

  Serial.print("Setting SF to ");
  Serial.println(SPREADING_FACTOR);
  setDataRate(SPREADING_FACTOR);
}

void setup()
{
  // Setup loraid access
  Serial.begin(9600);

  // reset OLED display via software
  /*pinMode(OLED_RST, OUTPUT);
  digitalWrite(OLED_RST, LOW);
  delay(20);
  digitalWrite(OLED_RST, HIGH);

  // initialize OLED
  Wire.begin(OLED_SDA, OLED_SCL);
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3c, false, false))
  { // Address 0x3C for 128x32
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ; // Don't proceed, loop forever
  }
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setTextSize(1);
  display.setCursor(0, 0);*/

  // initialize LCD
  lcd.init();
  // turn on LCD backlight                      
  lcd.backlight();
  lcd.setCursor(0, 0);

  while (!Serial)
    ;
  if (!lora.init())
  {
    Serial.println("RFM95 not detected");
    delay(5000);
    return;
  }

#define EU_868
  // Set LoRaWAN Class change CLASS_A or CLASS_C
  lora.setDeviceClass(CLASS_A);

  // Set Data Rate
  lora.setDataRate(SF12BW125);
  //int minute = gps.time.minute();
  //setNextSF(minute);

  // set channel to random
  lora.setChannel(MULTI);

  // Put OTAA Key and DevAddress here
  lora.setDevEUI(devEui);
  lora.setAppEUI(appEui);
  lora.setAppKey(appKey);

  // Join procedure
  bool isJoined;
  do
  {
    Serial.println("Joining...");
    //display.println("Joining...");
    //display.display();
    lcd.print("Joining...");
    lcd.setCursor(0,cursorRow++);
    isJoined = lora.join();
    // wait for 6s to try again
    delay(6000);
  } while (!isJoined);
  Serial.println("Joined to network");
  //display.println("Joined to network");
  //display.display();
  lcd.clear();
  cursorRow = 0;
  lcd.setCursor(0, 0);
  lcd.print("Joined Network!");
  uint8_t payloadBuffer[10];
  payloadBuffer[0] = 'c';
  lora.sendUplink(payloadBuffer, 1, 0, 10);
}

void loop()
{ 
  if(millis() - prevMillis > uplinkInterval){
    uint8_t payloadBuffer[10];
    payloadBuffer[0] = 'c';
    lora.sendUplink(payloadBuffer, 1, 0, 10);
    Serial.print("sent uplink");
    prevMillis = millis();
  }
  
  recvStatus = lora.readData(outBuff);
  if (recvStatus)
  {
    //display.clearDisplay();
    //display.setCursor(0, 0);
    lcd.clear();
    cursorRow = 0;
    lcd.setCursor(0, 0);
    Serial.println("LoRa packet received");
    int newLineCount = 0;
    String arr[4] = {"", "Lat: ", "Lng: ", "Fix type: "};
    //display.print(arr[newLineCount]);
    for (int i = 0; i < sizeof(outBuff); i++)
    {
      if (outBuff[i] >= 32 && outBuff[i] <= 126)
      {
        char c = outBuff[i];
        //display.print(c);
        //display.display();
        lcd.print(c);
        if (i > 0)
        {
          if (outBuff[i - 1] == 10 /*new line*/ && newLineCount == 3) // if the current char is the fix type (last char) then break the loop cuz sometimes some garbage gets printed after the fix type
          {
            break;
          }
        }
      }
      if (outBuff[i] == 10) // if newline character
      {
        newLineCount++;
        char c = outBuff[i];
        //display.print(c);
        //display.print(c);
        //display.print(arr[newLineCount]);
        //display.display();
        cursorRow++;
        lcd.setCursor(0, cursorRow);
        lcd.print(arr[newLineCount]);
      }
    }
    //display.print("SF: ");
    //display.println(SPREADING_FACTOR);
    Serial.println();
  }

  // Check Lora RX
  lora.update();
}